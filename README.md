# Bookmark

A simple bookmarking script.

## Requirements

  - Dmenu (Rofi is even better)
  - i3

## Installation

Start by sourcing the script in your `.bashrc` or `.profile`:

```
source path/to/bookmarks.sh
```

Then add to your `.config/i3/config` the following keybind:

```
bindsym $mod+o exec --no-startup-id ". ~/bin/bookmarks.sh; bookmark_go"
```

## Usage

To bookmark a location, use `bookmark <alias>` from your terminal.

Use <mod>+o to open a terminal using a Bookmark.

# License

MIT