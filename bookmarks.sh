#!/bin/sh

bookmark_go() {
  CWD=$(dmenu < ~/.bookmarks | awk '{ print substr($0, match($0, /:\s*/)+1, length()) }')
  CWD="${CWD/\~/$HOME}" 

  [[ -z "$CWD" ]] && { exit 1; }

  echo $CWD
  i3-sensible-terminal -d $CWD
}

bookmark() {
  ALIAS=$1
  BOOKMARK=$(pwd)

  if grep -q "$BOOKMARK" ~/.bookmarks; then
    echo "$BOOKMARK is already bookmarked."
    return 1
  fi

  if [ -n "$ALIAS" ]; then
    BOOKMARK="$ALIAS\:\t\t\t$BOOKMARK"
  fi

  echo "Bookmarked $BOOKMARK."
  echo "$BOOKMARK" >> ~/.bookmarks
}
